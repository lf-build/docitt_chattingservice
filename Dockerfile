FROM registry.lendfoundry.com/base:beta8

ADD . /app
WORKDIR /app/src/docitt.ChattingService.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

WORKDIR /app/src/docitt.ChattingService
RUN eval "$CMD_RESTORE"
RUN dnu build

WORKDIR /app/src/docitt.ChattingService.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel