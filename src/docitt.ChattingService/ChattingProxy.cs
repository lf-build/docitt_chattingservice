﻿using System;
using System.Threading.Tasks;
using docitt.ChattingService.Configuration;
using RestSharp;

namespace docitt.ChattingService
{
    public class ChattingProxy : IChattingProxy
    {
        private IChattingConfiguration Configuration { get; }

        public ChattingProxy(IChattingConfiguration configuration)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            if (string.IsNullOrWhiteSpace(configuration.ApzAppId))
            {
                throw new ArgumentException("Application Id cannot be empty", nameof(configuration.ApzAppId));
            }

            if (string.IsNullOrWhiteSpace(configuration.BaseUrl))
            {
                throw new ArgumentException("BaseUrl cannot be empty", nameof(configuration.BaseUrl));
            }

            if (string.IsNullOrWhiteSpace(configuration.ApzToken))
            {
                throw new ArgumentException("ApzToken cannot be empty", nameof(configuration.ApzToken));
            }

            if (string.IsNullOrWhiteSpace(configuration.Authorization))
            {
                throw new ArgumentException("Authorization cannot be empty", nameof(configuration.Authorization));
            }

            Configuration = configuration;
        }
      /// <summary>
      /// Get the message history between to and form user from applozic third party
      /// </summary>
      /// <param name="toUserId"></param>
      /// <param name="fromUserId"></param>
      /// <param name="startIndex"></param>
      /// <returns>Return the message list.</returns>
        public Task<MessageResultList> GetMessageHistory(string toUserId,string fromUserId, int startIndex)
        {
            if (string.IsNullOrEmpty(toUserId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(toUserId));

            if (string.IsNullOrEmpty(fromUserId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(fromUserId));

            //only get six month messages.
            Int64 endTime = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            Int64 startTime = (Int64)DateTime.UtcNow.AddMonths(-Configuration.ArchiveDuration).Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
            
            var restRequest = new RestRequest(Method.GET);
            int pageSize = 20;
            if(Configuration.PageSize > 0)
            {
                pageSize = Configuration.PageSize;
            }

            restRequest.AddParameter("Apz-AppId", Configuration.ApzAppId, ParameterType.HttpHeader);
            restRequest.AddParameter("Apz-Token", Configuration.Authorization + " "+ Configuration.ApzToken, ParameterType.HttpHeader);
            restRequest.AddParameter("Content-Type", "application/json", ParameterType.HttpHeader);

            restRequest.AddQueryParameter("userId", toUserId);
            restRequest.AddQueryParameter("startIndex", Convert.ToString(startIndex));
            restRequest.AddQueryParameter("pageSize", Convert.ToString(pageSize));
            restRequest.AddQueryParameter("ofUserId", Convert.ToString(fromUserId));
            restRequest.AddQueryParameter("startTime", Convert.ToString(startTime));
            restRequest.AddQueryParameter("endTime", Convert.ToString(endTime));

            string url = Configuration.BaseUrl + "message/list";
                        
            var objResult = ExecuteAsync<MessageResultList>(restRequest, url);

            return objResult;
        }

       /// <summary>
       /// Register the user into applozic third party app.
       /// </summary>
       /// <param name="userRequest">User request information</param>
       /// <returns>User registration response object</returns>
        public Task<UserRegistrationResponse> RegisterUser(IUserRegistrationRequest userRequest)
        {
            if (string.IsNullOrWhiteSpace(userRequest.userId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userRequest.userId));

            if (string.IsNullOrWhiteSpace(userRequest.email))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userRequest.email));
      
            var restRequest = new RestRequest(Method.POST);
         
            restRequest.AddParameter("Apz-AppId", Configuration.ApzAppId, ParameterType.HttpHeader);
            restRequest.AddParameter("Apz-Token", Configuration.Authorization + " " + Configuration.ApzToken, ParameterType.HttpHeader);
            restRequest.AddParameter("Content-Type", "application/json", ParameterType.HttpHeader);



            userRequest.applicationId = Configuration.ApzAppId;
               
            var userRegistrationRequest = restRequest.JsonSerializer.Serialize(userRequest);

            restRequest.AddParameter("application/json",userRegistrationRequest, ParameterType.RequestBody);

            string url = Configuration.BaseUrl + "register/client";

            var objResult = ExecuteAsync<UserRegistrationResponse>(restRequest, url);

            return objResult;
        }



        /// <summary>
        ///  Execute async rest request
        /// </summary>
        /// <typeparam name="T">T object</typeparam>
        /// <param name="request">RestRequest object</param>
        /// <param name="url">URL of the api</param>
        /// <returns>Return T object</returns>
        public Task<T> ExecuteAsync<T>(RestRequest request, string url) where T : new()
        {
            var client = new RestClient();
            var taskResult = new TaskCompletionSource<T>();
            client.BaseUrl = new Uri(url);
            client.ExecuteAsync<T>(request, response =>
            {
                taskResult.SetResult(response.Data);
            }
            );
            
            return taskResult.Task;
        }
    }
}
