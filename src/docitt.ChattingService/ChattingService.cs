﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docitt.ChattingService
{
    public class ChattingService : IChattingService
    {
        private IChattingProxy ChattingProxy { get; }

        public ChattingService(IChattingProxy chattingProxy)
        {
            ChattingProxy = chattingProxy;
        }
        public Task<MessageResultList> GetMessageHistory(string toUserId, string fromUserId, int startIndex)
        {
            if (string.IsNullOrEmpty(toUserId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(toUserId));

            if (string.IsNullOrEmpty(fromUserId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(fromUserId));

            return ChattingProxy.GetMessageHistory(toUserId, fromUserId, startIndex);
        }

        /// <summary>
        /// Register the user into applozic third party app.
        /// </summary>
        /// <param name="userRequest">User request information</param>
        /// <returns>User registration response object</returns>
        public Task<UserRegistrationResponse> RegisterUser(IUserRegistrationRequest userRequest)
        {
            return ChattingProxy.RegisterUser(userRequest);
        }
    }
}
