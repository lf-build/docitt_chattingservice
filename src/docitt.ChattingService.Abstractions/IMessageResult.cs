﻿namespace docitt.ChattingService
{
    public interface IMessageResult
    {
        string createdAtTime { get; set; }
        bool Delivered { get; set; }
        string Message { get; set; }
        bool Read { get; set; }
        bool Sent { get; set; }
        int Type { get; set; }
    }
}