﻿namespace docitt.ChattingService.Configuration
{
    public interface IChattingConfiguration
    {
        string ApzAppId { get; set; }
        string ApzToken { get; set; }
        string Authorization { get; set; }
        string BaseUrl { get; set; }

        int ArchiveDuration { get; set; }  //in months

        int PageSize { get; set; }
    }
}