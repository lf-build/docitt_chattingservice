﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docitt.ChattingService.Configuration
{
    public class ChattingConfiguration : IChattingConfiguration
    {
        public string ApzAppId { get; set; }
        public string ApzToken { get; set; }
        public string Authorization { get; set; } //Basic , berrer etc...

        public string BaseUrl { get; set; }

        public int ArchiveDuration { get; set; } //in months

        public int PageSize { get; set; }

    }
}
