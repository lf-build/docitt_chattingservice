﻿using LendFoundry.Foundation.Services.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docitt.ChattingService
{
    public static class Settings
    {
        public static string ServiceName { get; } = "ChattingService";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

       // public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static string Nats { get; } = Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}
