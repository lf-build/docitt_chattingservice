﻿namespace docitt.ChattingService
{
    public interface IUserRegistrationResponse
    {
        string ContactNumber { get; set; }
        string CurrentTimeStamp { get; set; }
        string deviceKey { get; set; }
        string LastSyncTime { get; set; }
        string Message { get; set; }
        string UserKey { get; set; }
    }
}