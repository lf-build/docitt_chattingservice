﻿

namespace docitt.ChattingService
{
    public class UserRegistrationRequest : IUserRegistrationRequest
    {
        public string userId { get; set; }

        public string displayName { get; set; }

        public string email { get; set; }

        public int contactNumber { get; set; }

        public string applicationId { get; set; }

        public string imageLink { get; set; }

        public string statusMessage { get; set; }
    }
}
