﻿using System.Threading.Tasks;

namespace docitt.ChattingService
{
    public interface IChattingService
    {
        Task<MessageResultList> GetMessageHistory(string toUserId, string fromUserId, int startIndex);

        /// <summary>
        /// Register the user into applozic third party app.
        /// </summary>
        /// <param name="userRequest">User request information</param>
        /// <returns>User registration response object</returns>
        Task<UserRegistrationResponse> RegisterUser(IUserRegistrationRequest userRequest);
    }
}