﻿using System.Threading.Tasks;
using RestSharp;

namespace docitt.ChattingService
{
    public interface IChattingProxy
    {
        Task<T> ExecuteAsync<T>(RestRequest request, string url) where T : new();
        Task<MessageResultList> GetMessageHistory(string toUserId, string fromUserId, int startIndex);

        /// <summary>
        /// Register the user into applozic third party app.
        /// </summary>
        /// <param name="userRequest">User request information</param>
        /// <returns>User registration response object</returns>
        Task<UserRegistrationResponse> RegisterUser(IUserRegistrationRequest userRequest);
    }
}
