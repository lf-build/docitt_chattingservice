﻿namespace docitt.ChattingService
{
    public interface IUserRegistrationRequest
    {
        int contactNumber { get; set; }
        string displayName { get; set; }
        string email { get; set; }
        string userId { get; set; }

        string applicationId { get; set; }

        string imageLink { get; set; }

        string statusMessage { get; set; }
    }
}