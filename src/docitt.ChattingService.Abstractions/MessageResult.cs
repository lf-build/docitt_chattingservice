﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docitt.ChattingService
{
    public class MessageResult : IMessageResult
    {
        public string Message { get; set; }
        public bool Sent { get; set; }
        public bool Delivered { get; set; }
        public bool Read { get; set; }
        public string createdAtTime { get; set; } //unix time stamp (seconds)
        public int Type { get; set; }

    }
}
