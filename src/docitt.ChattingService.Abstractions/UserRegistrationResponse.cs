﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docitt.ChattingService
{
    public class UserRegistrationResponse
    {
        public string Message { get; set; }
        public string UserKey { get; set; }
        public string deviceKey { get; set; }
        public string LastSyncTime { get; set; }
        public string ContactNumber { get; set; }
        public string CurrentTimeStamp { get; set; }

    }
}
