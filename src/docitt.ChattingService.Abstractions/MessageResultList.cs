﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace docitt.ChattingService
{
    public class MessageResultList
    {
        public List<MessageResult> message { get; set; }
    }
}
