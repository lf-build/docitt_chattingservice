﻿using docitt.ChattingService;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Threading.Tasks;

namespace docitt.ChattingService.Api.Controllers
{
    public class ApiController : ExtendedController
    {
        private IChattingService _ChattingService { get; }

        public ApiController(IChattingService chattingService, ILogger logger) : base(logger)
        {
            if (chattingService == null)
                throw new ArgumentException($"{nameof(chattingService)} is mandatory");

            _ChattingService = chattingService;
        }

        /// <summary>
        /// GetMessageHistory
        /// </summary>
        /// <param name="toUserId">toUserId</param>
        /// <param name="fromUserId">fromUserId</param>
        /// <param name="startIndex">startIndex</param>
        /// <returns>MessageResultList</returns>
        [HttpGet("{toUserId}/{fromUserId}/{startIndex}/gethistory")]
        [ProducesResponseType(typeof(MessageResultList), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetMessageHistory(string toUserId, string fromUserId, int startIndex)
        {

            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await _ChattingService.GetMessageHistory(toUserId, fromUserId, startIndex));
                }              
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }

        /// <summary>
        /// RegisterUser
        /// </summary>
        /// <param name="objUser">objUser</param>
        /// <returns>NoContentResult</returns>
        [HttpPost("register/client")]
        [ProducesResponseType(typeof(UserRegistrationResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> RegisterUser([FromBody] UserRegistrationRequest objUser)
        {

            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await _ChattingService.RegisterUser(objUser));
                }
                catch (Exception ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
    }
}
