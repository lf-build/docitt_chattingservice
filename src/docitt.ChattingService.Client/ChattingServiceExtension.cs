﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace docitt.ChattingService.Client
{
    public static class ChattingServiceExtension
    {
        public static IServiceCollection AddChattingService(this IServiceCollection services, string endpoint,
           int port = 5000)
        {
            services.AddTransient<IChattingServiceFactory>(p => new ChattingServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IChattingServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
