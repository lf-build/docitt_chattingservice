﻿using LendFoundry.Foundation.Services;
using RestSharp;

using System.Threading.Tasks;

namespace docitt.ChattingService.Client
{
    public class ChattingService : IChattingService
    {
        private IServiceClient Client { get; }

        public ChattingService(IServiceClient client)
        {
            Client = client;
        }


        public async Task<MessageResultList> GetMessageHistory(string toUserId, string fromUserId, int startIndex)
        {
            var request = new RestRequest("{toUserId}/{fromUserId}/{startIndex}/gethistory", Method.GET);
            request.AddUrlSegment("toUserId", "suresh");
            request.AddUrlSegment("fromUserId", "vivek");
            request.AddUrlSegment("startIndex", "0");            
            return await Client.ExecuteAsync<MessageResultList>(request);
        }

        public async Task<UserRegistrationResponse> RegisterUser(IUserRegistrationRequest userRequest)
        {
            var request = new RestRequest("register/client", Method.POST);
             request.AddParameter("application/json", userRequest, ParameterType.RequestBody);
            return await Client.ExecuteAsync<UserRegistrationResponse>(request);
        }
    }
}
