﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;


namespace docitt.ChattingService.Client
{
    public class ChattingServiceFactory : IChattingServiceFactory
    {

        public ChattingServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IChattingService Create(ITokenReader reader)
        {
            var client = Provider.GetServiceClient(reader, Endpoint, Port);

            return new ChattingService(client);

        }
    }
}
