﻿using LendFoundry.Security.Tokens;

namespace docitt.ChattingService.Client
{
    public interface IChattingServiceFactory
    {
        IChattingService Create(ITokenReader reader);
    }
}
