﻿using System;
using Moq;
using Xunit;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace docitt.ChattingService.Tests
{
    public class ChattingSerticeTests
    {      
        private Mock<IChattingProxy> chattingProxy { get; }
        private IChattingService chattingService { get; }

      
        public ChattingSerticeTests()
        {
            chattingProxy = new Mock<IChattingProxy>();
            chattingService = new ChattingService(chattingProxy.Object);
        }

        [Fact]
        public async void GetMessageHistory_WithNullOrEmptytoUserId_FromUserid_ThrowsArgumentException()
        {
          
            await Assert.ThrowsAsync<ArgumentException>(() => chattingService.GetMessageHistory("", "test",2));
            await Assert.ThrowsAsync<ArgumentException>(() => chattingService.GetMessageHistory(null, "test", 2));
            await Assert.ThrowsAsync<ArgumentException>(() => chattingService.GetMessageHistory(string.Empty, "test", 2));

            await Assert.ThrowsAsync<ArgumentException>(() => chattingService.GetMessageHistory("suresh", "", 2));
            await Assert.ThrowsAsync<ArgumentException>(() => chattingService.GetMessageHistory("suresh", null, 2));
            await Assert.ThrowsAsync<ArgumentException>(() => chattingService.GetMessageHistory("suresh", string.Empty, 2));
        }

        [Fact]
        public async void GetMessageHistory_WithValidData_SuccessWithData()
        {
            chattingProxy.Setup(x => x.GetMessageHistory(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<int>()))
            .ReturnsAsync(new MessageResultList());

          var result = await chattingService.GetMessageHistory("test", "suresh", 0);

            Assert.NotNull(result);            
            Assert.IsType<MessageResultList>(result);
        }       
    }
}
