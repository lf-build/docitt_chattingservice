﻿using System;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;
using docitt.TwilioOTP;
using docitt.TwilioOTP.Api.Controllers;
using System.Threading.Tasks;
using System.Collections.Generic;
using LendFoundry.EventHub.Client;
using docitt.ChattingService;

namespace LendFoundry.Email.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<IChattingProxy> chattingProxy { get; }
        private Mock<IChattingService> chattingService { get; }
        private ApiController apiController { get; set; }



        public ApiControllerTests()
        {
            chattingProxy = new Mock<IChattingProxy>();
            chattingService = new Mock<IChattingService>();
            apiController = new ApiController(chattingService.Object);
        }

        [Fact]
        public void Controller_With_Null_Services_Should_Throw_ArgumentException()
        {
            Assert.Throws<ArgumentException>(() => new ApiController(null));
        }

        [Fact]
        public async void GetMessageHistory_WithNullOrEmptytoUserId_FromUserId_ThrowsArgumentException()
        {
            
            string toUserId = "";
                var fromUserId = "suresh";
            var startIndex = 2;
           var chattingService1 = new ChattingService(chattingProxy.Object);
            apiController = new ApiController(chattingService1);
            //await Assert.ThrowsAsync<ArgumentException>(() => apiController.GetMessageHistory(toUserId, fromUserId, startIndex));
            var result = (ErrorResult)await apiController.GetMessageHistory(toUserId, fromUserId, startIndex);

            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

            toUserId = string.Empty;
            result = (ErrorResult)await apiController.GetMessageHistory(toUserId, fromUserId, startIndex);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
            toUserId = "test";
            fromUserId = "";

            result = (ErrorResult)await apiController.GetMessageHistory(toUserId, fromUserId, startIndex);
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }


        [Fact]
        public async void Controller_GetMessageHistory_Returns_Result_OnSuccess()
        {
            chattingService.Setup(x => x.GetMessageHistory(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<int>()))
                .ReturnsAsync(new MessageResultList());

            var result = (HttpOkObjectResult)await apiController.GetMessageHistory("test","suresh",0);

            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<MessageResultList>(result.Value);
        }



    }
}
