﻿
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System.Threading.Tasks;
using Xunit;

namespace docitt.ChattingService.Client.Tests
{
    public class ChattingServiceClientTests
    {
        private IChattingService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public ChattingServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new ChattingService(ServiceClient.Object);
        }

        [Fact]
        public async void Client_GetMessageHistory()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<MessageResultList>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => Request = r)
               .ReturnsAsync(new MessageResultList());

            var result = await Client.GetMessageHistory("suresh", "test", 2);

            ServiceClient.Verify(x => x.ExecuteAsync<MessageResultList>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("{toUserId}/{fromUserId}/{startIndex}/gethistory", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);

           
        }


        [Fact]
        public async void Client_RegisterClient()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<UserRegistrationResponse>(It.IsAny<IRestRequest>()))
               .Callback<IRestRequest>(r => Request = r)
               .ReturnsAsync(new UserRegistrationResponse());

            var result = await Client.RegisterUser(new UserRegistrationRequest());

            ServiceClient.Verify(x => x.ExecuteAsync<UserRegistrationResponse>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("register/client", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);


        }


    }
}
